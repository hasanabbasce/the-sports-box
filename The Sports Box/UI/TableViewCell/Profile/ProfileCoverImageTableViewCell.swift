//
//  ProfileCoverImageTableViewCell.swift
//  The Sports Box
//
//  Created by Admin on 24/05/18.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ProfileCoverImageTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCoverPhoto: UIImageView!
    @IBOutlet weak var imgProfilePhoto: UIImageView!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblJerseyNumber: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
