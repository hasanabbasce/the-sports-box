//
//  BannerView.swift
//  The Sports Box
//
//  Created by Admin on 25/05/18.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class BannerView: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imgBannerText: UIImageView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnAction: UIButton! // Will be having different background based upon ViewController
    
   
    
    var bannerType: NSInteger?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit()
    {
        Bundle.main.loadNibNamed("BannerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        
        //contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction func searchPressed()
    {
        print("Banner Type = " + String(bannerType!))
        
    }
}
