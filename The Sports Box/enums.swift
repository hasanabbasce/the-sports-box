//
//  enums.swift
//  The Sports Box
//
//  Created by Admin on 26/05/18.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

enum BannerType :NSInteger
{
    case Home = 1
    case Leagues
    case Teams
    case Profile
}
