//
//  SignupViewController.swift
//  The Sports Box
//
//  Created by Hasan Abbas on 24/04/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SignupViewController: UIViewController, UINavigationControllerDelegate , UIImagePickerControllerDelegate, UIPopoverPresentationControllerDelegate,NVActivityIndicatorViewable {
    
    @IBOutlet weak var firstname: UITextField!
    @IBOutlet weak var lastname: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repassword: UITextField!
    @IBOutlet weak var gender: UISegmentedControl!
    @IBOutlet weak var profilepic: UIImageView!
    
    var pictureData:Data?
    
    let activityData = ActivityData()
   
    var networkingService = NetworkingService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTapGestureRecognizerOnProfilePic()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Signup(_ sender: Any) {
        if(validatefields())
        {
            var newuser = Useri.init(initdic: [:])
            newuser.email = self.email.text
            newuser.fullname = self.firstname.text! + " " + self.lastname.text!
            newuser.firstname = self.firstname.text
            newuser.lastname = self.lastname.text
            newuser.gender = self.gender.selectedSegmentIndex == 0 ? "Male" : "Female"
            newuser.post_date = NSDate().timeIntervalSince1970 as NSNumber
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if(appDelegate.currentLocation != nil)
            {
                newuser.lat = appDelegate.currentLocation?.coordinate.latitude
                newuser.lng = appDelegate.currentLocation?.coordinate.longitude
                newuser.state = appDelegate.administrativeArea
                newuser.country = appDelegate.country
                newuser.city = appDelegate.locality
                newuser.location = appDelegate.administrativeArea! + " " + appDelegate.country! + " " + appDelegate.locality!
                newuser.postal_code = appDelegate.postalCode
            }
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            networkingService.signUpApp(user: newuser, pictureData: self.pictureData, password: self.password.text, completion: { (response) in
                
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                print(response)
                if(response["Data"] != nil)
                {
                    
                }
                if(response["Error"] != nil)
                {
                    let alert = UIAlertController(title: "Error Occured ", message: response["Error"] as! String  , preferredStyle: UIAlertControllerStyle.alert)
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            print("dissmis alert")
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                        }}))
                }
            })
        }
        
    }
    
    
    
    func setTapGestureRecognizerOnProfilePic(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.chooseProfilePic))
        tapGesture.numberOfTapsRequired = 1
        self.profilepic.addGestureRecognizer(tapGesture)
        
    }
    
    
    func validatefields() ->Bool
    {
        let firstnameval = firstname.text
        let lastnameval = lastname.text
        let emailval = email.text
        let passwordval = password.text
        let repasswordval = repassword.text
        var noerror : Bool = true
        var error_message : String = ""
        if(firstnameval == "" && noerror)
        {
            noerror = false
            error_message = "First Name required"
            
        }
        if(lastnameval == "" && noerror)
        {
            noerror = false
            error_message = "Last Name required"
            
        }
        else if(emailval  == "" && noerror)
        {
            noerror = false
            error_message = "Email required"
            
        }
        else if(!isValidEmail(testStr: emailval!) && noerror)
        {
            noerror = false
            error_message = "Please input correct Email address "
            
        }
        else if(passwordval  == "" && noerror)
        {
            noerror = false
            error_message = "Password required"
        }
        else if(repasswordval  == "" && noerror)
        {
            noerror = false
            error_message = "Re-Password required"
        }
        else if(repasswordval  != passwordval && noerror)
        {
            noerror = false
            error_message = "Password doesn't match Re-Password"
        }
        
        
        if(!noerror)
        {
            let alert = UIAlertController(title: "Error", message: error_message, preferredStyle: UIAlertControllerStyle.alert)
            
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("ok")
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            return false
        }
        else
        {
            return true
        }
        //return false
    }
    
    
    @objc private func chooseProfilePic()
    {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.modalPresentationStyle = .popover
        pickerController.popoverPresentationController?.delegate = self
        pickerController.popoverPresentationController?.sourceView = profilepic
        let alertController = UIAlertController(title: "Add a Picture", message: "Choose From", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            pickerController.sourceType = .camera
            self.present(pickerController, animated: true, completion: nil)
            
        }
        let photosLibraryAction = UIAlertAction(title: "Photos Library", style: .default) { (action) in
            pickerController.sourceType = .photoLibrary
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let savedPhotosAction = UIAlertAction(title: "Saved Photos Album", style: .default) { (action) in
            pickerController.sourceType = .savedPhotosAlbum
            self.present(pickerController, animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photosLibraryAction)
        alertController.addAction(savedPhotosAction)
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
            //  popoverController.sourceView = self.view
            //  popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            //  popoverController.permittedArrowDirections = []
            popoverController.sourceView = profilepic as? UIView
            popoverController.sourceRect = (profilepic as AnyObject).bounds;
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.left
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.profilepic.image = chosenImage
            self.pictureData = UIImageJPEGRepresentation(self.profilepic.image!, 0.2)!
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
