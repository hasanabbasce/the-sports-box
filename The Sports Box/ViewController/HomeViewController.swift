//
//  HomeViewController.swift
//  The Sports Box
//
//  Created by Hasan Abbas on 24/04/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var bannerView: BannerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.imgBannerText.image = UIImage(named: "my_dash_banner_text")
        bannerView.bannerType = BannerType.Home.rawValue
        bannerView.btnAction.setBackgroundImage(UIImage.init(named: "bell_icon"), for: UIControlState.normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
