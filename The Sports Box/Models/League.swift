//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct League {
    
    var lid: String?
    var uid: String?
    var organization: String?
    var tournament_name: String?
    var league_logo: String?
    var league_name_lowercase:String?
    var league_name:String?
    var about: String?
    var sport: String?
    var sponsor: String?
    var lvlofplay: String?
    var start_date: String?
    var end_date: String?
    var gameDay: String?
    var gameTime: String?
    var gamePerSeasion: String?
    var gender: String?
    var age: String?
    var player_per_team_max: String?
    var rules: String?
    var payment_method: String?
    var cover:String?
    var duration:String?
    var defaultt:Bool?
    
    var ref: DatabaseReference?
    var key: String = ""
    
    
    var location: String?
    
    var lat: Double?
    var lng: Double?
    var locality : String?
    var postalCode : String?
    var administrativeArea : String?
    var country : String?
    var postal_code : String?
    var route : String?
    var city : String?
    var state : String?
    
    var postDate: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
        
        
        self.lid = (snapshot.value as! NSDictionary)["lid"] as? String ?? ""
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.organization = (snapshot.value as! NSDictionary)["organization"] as? String ?? ""
        self.tournament_name = (snapshot.value as! NSDictionary)["tournament_name"] as? String ?? ""
        self.league_logo = (snapshot.value as! NSDictionary)["league_logo"] as? String ?? ""
        self.league_name_lowercase = (snapshot.value as! NSDictionary)["league_name_lowercase"] as? String ?? ""
        self.league_name = (snapshot.value as! NSDictionary)["league_name"] as? String ?? ""
        self.about = (snapshot.value as! NSDictionary)["about"] as? String ?? ""
        self.sport = (snapshot.value as! NSDictionary)["sport"] as? String ?? ""
        self.lvlofplay = (snapshot.value as! NSDictionary)["lvlofplay"] as? String ?? ""
        self.start_date = (snapshot.value as! NSDictionary)["start_date"] as? String ?? ""
        self.end_date = (snapshot.value as! NSDictionary)["end_date"] as? String ?? ""
        self.gameDay = (snapshot.value as! NSDictionary)["gameDay"] as? String ?? ""
        self.gameTime = (snapshot.value as! NSDictionary)["gameTime"] as? String ?? ""
        self.gamePerSeasion = (snapshot.value as! NSDictionary)["gamePerSeasion"] as? String ?? ""
        self.gender = (snapshot.value as! NSDictionary)["gender"] as? String ?? ""
        self.age = (snapshot.value as! NSDictionary)["age"] as? String ?? ""
        self.player_per_team_max = (snapshot.value as! NSDictionary)["player_per_team_max"] as? String ?? ""
        self.rules = (snapshot.value as! NSDictionary)["rules"] as? String ?? ""
        self.payment_method = (snapshot.value as! NSDictionary)["payment_method"] as? String ?? ""
        self.cover = (snapshot.value as! NSDictionary)["cover"] as? String ?? ""
        self.duration = (snapshot.value as! NSDictionary)["duration"] as? String ?? ""
        self.defaultt = (snapshot.value as! NSDictionary)["default"] as? Bool ?? false
        
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.location = (snapshot.value as! NSDictionary)["location"] as? String ?? ""
        self.lat = (snapshot.value as! NSDictionary)["lat"] as? Double ?? 0
        self.lng = (snapshot.value as! NSDictionary)["lng"] as? Double ?? 0
        self.locality = (snapshot.value as! NSDictionary)["locality"] as? String ?? ""
        self.postalCode = (snapshot.value as! NSDictionary)["postalCode"] as? String ?? ""
        self.administrativeArea = (snapshot.value as! NSDictionary)["administrativeArea"] as? String ?? ""
        self.country = (snapshot.value as! NSDictionary)["country"] as? String ?? ""
        self.postal_code = (snapshot.value as! NSDictionary)["postal_code"] as? String ?? ""
        self.route = (snapshot.value as! NSDictionary)["route"] as? String ?? ""
        self.city = (snapshot.value as! NSDictionary)["city"] as? String ?? ""
        self.state = (snapshot.value as! NSDictionary)["state"] as? String ?? ""
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        
        if(initdic["lid"] != nil)
        {
            self.lid = initdic["lid"]! as? String
        }
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["organization"] != nil)
        {
            self.organization = initdic["organization"]! as? String
        }
        if(initdic["tournament_name"] != nil)
        {
            self.tournament_name = initdic["tournament_name"]! as? String
        }
        if(initdic["league_logo"] != nil)
        {
            self.league_logo = initdic["league_logo"]! as? String
        }
        if(initdic["league_name_lowercase"] != nil)
        {
            self.league_name_lowercase = initdic["league_name_lowercase"]! as? String
        }
        if(initdic["league_name"] != nil)
        {
            self.league_name = initdic["league_name"]! as? String
        }
        if(initdic["about"] != nil)
        {
            self.about = initdic["about"]! as? String
        }
        if(initdic["sport"] != nil)
        {
            self.sport = initdic["sport"]! as? String
        }
        if(initdic["lvlofplay"] != nil)
        {
            self.lvlofplay = initdic["lvlofplay"]! as? String
        }
        if(initdic["start_date"] != nil)
        {
            self.start_date = initdic["start_date"]! as? String
        }
        if(initdic["end_date"] != nil)
        {
            self.end_date = initdic["end_date"]! as? String
        }
        if(initdic["gameDay"] != nil)
        {
            self.gameDay = initdic["gameDay"]! as? String
        }
        if(initdic["gameTime"] != nil)
        {
            self.gameTime = initdic["gameTime"]! as? String
        }
        if(initdic["gamePerSeasion"] != nil)
        {
            self.gamePerSeasion = initdic["gamePerSeasion"]! as? String
        }
        if(initdic["gender"] != nil)
        {
            self.gender = initdic["gender"]! as? String
        }
        if(initdic["age"] != nil)
        {
            self.age = initdic["age"]! as? String
        }
        if(initdic["player_per_team_max"] != nil)
        {
            self.player_per_team_max = initdic["player_per_team_max"]! as? String
        }
        if(initdic["rules"] != nil)
        {
            self.rules = initdic["rules"]! as? String
        }
        if(initdic["route"] != nil)
        {
            self.route = initdic["route"]! as? String
        }
        if(initdic["payment_method"] != nil)
        {
            self.payment_method = initdic["payment_method"]! as? String
        }
        if(initdic["cover"] != nil)
        {
            self.cover = initdic["cover"]! as? String
        }
        if(initdic["duration"] != nil)
        {
            self.duration = initdic["duration"]! as? String
        }
        if(initdic["default"] != nil)
        {
            self.defaultt = initdic["default"]! as? Bool
        }
        
        
        
        
        if(initdic["location"] != nil)
        {
            self.location = initdic["location"]! as? String
        }
        if(initdic["lat"] != nil)
        {
            self.lat = initdic["lat"]! as? Double
        }
        if(initdic["lon"] != nil)
        {
            self.lng = initdic["lng"]! as? Double
        }
        if(initdic["locality"] != nil)
        {
            self.locality = initdic["locality"]! as? String
        }
        if(initdic["postalCode"] != nil)
        {
            self.postalCode = initdic["postalCode"]! as? String
        }
        if(initdic["administrativeArea"] != nil)
        {
            self.administrativeArea = initdic["administrativeArea"]! as? String
        }
        if(initdic["country"] != nil)
        {
            self.country = initdic["country"]! as? String
        }
        if(initdic["postal_code"] != nil)
        {
            self.postal_code = initdic["postal_code"]! as? String
        }
        if(initdic["city"] != nil)
        {
            self.city = initdic["city"]! as? String
        }
        if(initdic["state"] != nil)
        {
            self.state = initdic["state"]! as? String
        }
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.lid != nil)
        {
            initdic["lid"] = self.lid!
        }
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.organization != nil)
        {
            initdic["organization"] = self.organization!
        }
        if(self.tournament_name != nil)
        {
            initdic["tournament_name"] = self.tournament_name!
        }
        if(self.league_logo != nil)
        {
            initdic["league_logo"] = self.league_logo!
        }
        if(self.league_name_lowercase != nil)
        {
            initdic["league_name_lowercase"] = self.league_name_lowercase!
        }
        if(self.league_name != nil)
        {
            initdic["league_name"] = self.league_name!
        }
        if(self.about != nil)
        {
            initdic["about"] = self.about!
        }
        if(self.sport != nil)
        {
            initdic["sport"] = self.sport!
        }
        if(self.lvlofplay != nil)
        {
            initdic["lvlofplay"] = self.lvlofplay!
        }
        if(self.start_date != nil)
        {
            initdic["start_date"] = self.start_date!
        }
        if(self.start_date != nil)
        {
            initdic["start_date"] = self.start_date!
        }
        if(self.end_date != nil)
        {
            initdic["end_date"] = self.end_date!
        }
        if(self.gameDay != nil)
        {
            initdic["gameDay"] = self.gameDay!
        }
        if(self.gameTime != nil)
        {
            initdic["gameTime"] = self.gameTime!
        }
        if(self.gamePerSeasion != nil)
        {
            initdic["gamePerSeasion"] = self.gamePerSeasion!
        }
        if(self.gender != nil)
        {
            initdic["gender"] = self.gender!
        }
        if(self.age != nil)
        {
            initdic["age"] = self.age!
        }
        if(self.player_per_team_max != nil)
        {
            initdic["player_per_team_max"] = self.player_per_team_max!
        }
        if(self.rules != nil)
        {
            initdic["rules"] = self.rules!
        }
        if(self.payment_method != nil)
        {
            initdic["payment_method"] = self.payment_method!
        }
        if(self.cover != nil)
        {
            initdic["cover"] = self.cover!
        }
        if(self.duration != nil)
        {
            initdic["duration"] = self.duration!
        }
        if(self.defaultt != nil)
        {
            initdic["default"] = self.defaultt!
        }
        
        
        
        
        if(self.location != nil)
        {
            initdic["location"] = self.location!
        }
        if(self.lat != nil)
        {
            initdic["lat"] = self.lat!
        }
        if(self.lng != nil)
        {
            initdic["lng"] = self.lng!
        }
        
        
        if(self.locality != nil)
        {
            initdic["locality"] = self.locality!
        }
        
        if(self.postalCode != nil)
        {
            initdic["postalCode"] = self.postalCode!
        }
        if(self.administrativeArea != nil)
        {
            initdic["administrativeArea"] = self.administrativeArea!
        }
        if(self.country != nil)
        {
            initdic["country"] = self.country!
        }
        if(self.postal_code != nil)
        {
            initdic["postal_code"] = self.postal_code!
        }
        if(self.city != nil)
        {
            initdic["city"] = self.city!
        }
        if(self.route != nil)
        {
            initdic["route"] = self.route!
        }
        if(self.state != nil)
        {
            initdic["state"] = self.state!
        }
        
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        return initdic
    }
}



