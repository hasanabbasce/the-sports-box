//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct Useri {
    
    var uid: String?
    var fullname: String?
    var fullname_lowercase: String?
    var firstname : String?
    var lastname: String?
    var email : String?
    var gender: String?
    var employer : String?
    var levelofplay: String?
    var sport: String?
    var profile_img: String?
    var free_agent : Bool?
    var cover_img : String?
    var about : String?
    
    
    var location: String?
    var country : String?
    var city : String?
    var state: String?
    var route: String?
    var postal_code: String?
    var lng: Double?
    var lat: Double?
    
    var post_date: NSNumber?
    
    
    var ref: DatabaseReference?
    var key: String = ""
    
    init?(snapshot: DataSnapshot){
        
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.fullname = (snapshot.value as! NSDictionary)["fullname"] as? String ?? ""
        self.fullname_lowercase = (snapshot.value as! NSDictionary)["fullname_lowercase"] as? String ?? ""
        self.firstname = (snapshot.value as! NSDictionary)["firstname"] as? String ?? ""
        self.lastname = (snapshot.value as! NSDictionary)["lastname"] as? String ?? ""
        self.email = (snapshot.value as! NSDictionary)["email"] as? String ?? ""
        
        self.gender = (snapshot.value as! NSDictionary)["gender"] as? String ?? ""
        self.employer = (snapshot.value as! NSDictionary)["employer"] as? String ?? ""
        self.levelofplay = (snapshot.value as! NSDictionary)["levelofplay"] as? String ?? ""
        self.sport = (snapshot.value as! NSDictionary)["sport"] as? String ?? ""
        self.profile_img = (snapshot.value as! NSDictionary)["profile_img"] as? String ?? ""
        self.free_agent = (snapshot.value as! NSDictionary)["free_agent"] as? Bool ?? false
        self.cover_img = (snapshot.value as! NSDictionary)["cover_img"] as? String ?? ""
        self.about = (snapshot.value as! NSDictionary)["about"] as? String ?? ""
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        
        self.location = (snapshot.value as! NSDictionary)["location"] as? String ?? ""
        self.country = (snapshot.value as! NSDictionary)["country"] as? String ?? ""
        self.city = (snapshot.value as! NSDictionary)["city"] as? String ?? ""
        self.state = (snapshot.value as! NSDictionary)["state"] as? String ?? ""
        self.route = (snapshot.value as! NSDictionary)["route"] as? String ?? ""
        self.postal_code = (snapshot.value as! NSDictionary)["postal_code"] as? String ?? ""
        self.lng = (snapshot.value as! NSDictionary)["lng"] as? Double ?? 0
        self.lat = (snapshot.value as! NSDictionary)["lat"] as? Double ?? 0
        self.post_date = (snapshot.value as! NSDictionary)["post_date"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["firstname"] != nil)
        {
            self.firstname = initdic["firstname"]! as? String
        }
        if(initdic["lastname"] != nil)
        {
            self.lastname = initdic["lastname"]! as? String
        }
        if(initdic["email"] != nil)
        {
            self.email = initdic["email"]! as? String
        }
        if(firstname != nil && self.lastname != nil)
        {
        self.fullname = self.firstname! + " " + self.lastname!
        }
        if(initdic["gender"] != nil)
        {
            self.gender = initdic["gender"]!  as? String
        }
        if(initdic["employer"] != nil)
        {
            self.employer = initdic["employer"]!  as? String
        }
        if(initdic["levelofplay"] != nil)
        {
            self.levelofplay = initdic["levelofplay"]!  as? String
        }
        if(initdic["sport"] != nil)
        {
            self.sport = initdic["sport"]!  as? String
        }
        if(initdic["profile_img"] != nil)
        {
            self.profile_img = initdic["profile_img"]!  as? String
        }
        if(initdic["free_agent"] != nil)
        {
            self.free_agent = initdic["free_agent"]!  as? Bool
        }
        if(initdic["cover_img"] != nil)
        {
            self.cover_img = initdic["cover_img"]!  as? String
        }
        if(initdic["about"] != nil)
        {
            self.about = initdic["about"]!  as? String
        }
        
        
        
        if(initdic["location"] != nil)
        {
            self.location = initdic["location"]! as? String
        }
        if(initdic["country"] != nil)
        {
            self.country = initdic["country"]! as? String
        }
        if(initdic["city"] != nil)
        {
            self.city = initdic["city"]! as? String
        }
        if(initdic["state"] != nil)
        {
            self.state = initdic["state"]! as? String
        }
        if(initdic["route"] != nil)
        {
            self.route = initdic["route"]! as? String
        }
        if(initdic["postal_code"] != nil)
        {
            self.postal_code = initdic["postal_code"]! as? String
        }
        if(initdic["lng"] != nil)
        {
            self.lng = initdic["lng"]! as? Double
        }
        if(initdic["lat"] != nil)
        {
            self.lat = initdic["lat"]! as? Double
        }
        if(initdic["post_date"] != nil)
        {
            self.post_date = initdic["post_date"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.firstname != nil)
        {
            initdic["firstname"] = self.firstname!
        }
        if(self.lastname != nil)
        {
            initdic["lastname"] = self.lastname!
            
        }
        initdic["fullname"] = self.firstname! + " " + self.lastname!
        initdic["fullname_lowercase"] = (self.firstname! + " " + self.lastname!).lowercased()
        
        
        if(self.email != nil)
        {
            initdic["email"] = self.email!
        }
        if(self.gender != nil)
        {
            initdic["gender"] = self.gender!
        }
        if(self.employer != nil)
        {
            initdic["employer"] = self.employer!
        }
        if(self.levelofplay != nil)
        {
            initdic["levelofplay"] = self.levelofplay!
        }
        if(self.sport != nil)
        {
            initdic["sport"] = self.sport!
        }
        
        if(self.profile_img != nil)
        {
            initdic["profile_img"] = self.profile_img!
        }
        
        if(self.free_agent != nil)
        {
            initdic["free_agent"] = self.free_agent!
        }
        if(self.cover_img != nil)
        {
            initdic["cover_img"] = self.cover_img!
        }
        if(self.about != nil)
        {
            initdic["about"] = self.about!
        }
        if(self.location != nil)
        {
            initdic["location"] = self.location!
        }
        
        if(self.country != nil)
        {
            initdic["country"] = self.country!
        }
        if(self.city != nil)
        {
            initdic["city"] = self.city!
        }
        if(self.state != nil)
        {
            initdic["state"] = self.state!
        }
        if(self.route != nil)
        {
            initdic["route"] = self.route!
        }
        if(self.postal_code != nil)
        {
            initdic["postal_code"] = self.postal_code!
        }
        if(self.lng != nil)
        {
            initdic["lng"] = self.lng!
        }
        if(self.lat != nil)
        {
            initdic["lat"] = self.lat!
        }
        
        if(self.post_date != nil)
        {
            initdic["post_date"] = self.post_date!
        }
        return initdic
    }
}

