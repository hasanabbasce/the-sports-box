//
//  ProfileDataTableViewCell.swift
//  The Sports Box
//
//  Created by Admin on 24/05/18.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ProfileDataTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
