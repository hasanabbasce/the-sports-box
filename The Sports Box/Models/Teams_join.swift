//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct Teams_join {
    
    var jtid: String?
    var tid: String?
    var uid: String?
    var jtstatus: String?
    
    var ref: DatabaseReference?
    var key: String = ""
    
    var postDate: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
        
        
        self.jtid = (snapshot.value as! NSDictionary)["jtid"] as? String ?? ""
        self.tid = (snapshot.value as! NSDictionary)["tid"] as? String ?? ""
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.jtstatus = (snapshot.value as! NSDictionary)["jtstatus"] as? String ?? ""
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.postDate = (snapshot.value as! NSDictionary)["postDate"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        
        if(initdic["jtid"] != nil)
        {
            self.jtid = initdic["jtid"]! as? String
        }
        if(initdic["tid"] != nil)
        {
            self.tid = initdic["tid"]! as? String
        }
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["jtstatus"] != nil)
        {
            self.jtstatus = initdic["jtstatus"]! as? String
        }
        
        if(initdic["postDate"] != nil)
        {
            self.postDate = initdic["postDate"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.jtid != nil)
        {
            initdic["jtid"] = self.jtid!
        }
        if(self.tid != nil)
        {
            initdic["tid"] = self.tid!
        }
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.jtstatus != nil)
        {
            initdic["jtstatus"] = self.jtstatus!
        }
        if(self.postDate != nil)
        {
            initdic["postDate"] = self.postDate!
        }
        return initdic
    }
}






