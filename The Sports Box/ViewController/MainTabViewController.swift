//
//  MainTabViewController.swift
//  The Sports Box
//
//  Created by Hasan Abbas on 24/04/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        renderdashboard()
    }
    
    
    func renderdashboard()
    {
        let button = UIButton.init(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "left_icon"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        button.imageView?.contentMode = UIViewContentMode.scaleToFill
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -25, 0, -10)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        // Do any additional setup after loading the view.
        
        let image : UIImage = UIImage(named: "dashboard")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
        
        
        let buttonsearchright = UIButton.init(type: .custom)
        //set image for button
        buttonsearchright.setImage(UIImage(named: "notifications"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        buttonsearchright.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        buttonsearchright.imageView?.contentMode = UIViewContentMode.scaleToFill
        buttonsearchright.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        let barButtonright = UIBarButtonItem(customView: buttonsearchright)
        
        
        let buttonsearchright2 = UIButton.init(type: .custom)
        //set image for button
        buttonsearchright2.setImage(UIImage(named: "search"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        buttonsearchright2.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        buttonsearchright2.imageView?.contentMode = UIViewContentMode.scaleToFill
        buttonsearchright2.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        let barButtonright2 = UIBarButtonItem(customView: buttonsearchright2)
        
        self.navigationItem.rightBarButtonItems = [barButtonright2,barButtonright]
    }
    
    func renderleague()
    {
        let button = UIButton.init(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "left_icon"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        button.imageView?.contentMode = UIViewContentMode.scaleToFill
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -25, 0, -10)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        // Do any additional setup after loading the view.
        
        let image : UIImage = UIImage(named: "league")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
        
        
        let buttonsearchright = UIButton.init(type: .custom)
        //set image for button
        buttonsearchright.setImage(UIImage(named: "add_icon"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        buttonsearchright.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        buttonsearchright.imageView?.contentMode = UIViewContentMode.scaleToFill
        buttonsearchright.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        let barButtonright = UIBarButtonItem(customView: buttonsearchright)
        
        
        let buttonsearchright2 = UIButton.init(type: .custom)
        //set image for button
        buttonsearchright2.setImage(UIImage(named: "search"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        buttonsearchright2.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        buttonsearchright2.imageView?.contentMode = UIViewContentMode.scaleToFill
        buttonsearchright2.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        let barButtonright2 = UIBarButtonItem(customView: buttonsearchright2)
        
        self.navigationItem.rightBarButtonItems = [barButtonright2,barButtonright]
    }
    
    func renderteam()
    {
        let button = UIButton.init(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "left_icon"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        button.imageView?.contentMode = UIViewContentMode.scaleToFill
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -25, 0, -10)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        // Do any additional setup after loading the view.
        
        let image : UIImage = UIImage(named: "league")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
        
        
        let buttonsearchright = UIButton.init(type: .custom)
        //set image for button
        buttonsearchright.setImage(UIImage(named: "add_icon"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        buttonsearchright.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        buttonsearchright.imageView?.contentMode = UIViewContentMode.scaleToFill
        buttonsearchright.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        let barButtonright = UIBarButtonItem(customView: buttonsearchright)
        
        
        let buttonsearchright2 = UIButton.init(type: .custom)
        //set image for button
        buttonsearchright2.setImage(UIImage(named: "search"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        buttonsearchright2.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        buttonsearchright2.imageView?.contentMode = UIViewContentMode.scaleToFill
        buttonsearchright2.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        let barButtonright2 = UIBarButtonItem(customView: buttonsearchright2)
        
        self.navigationItem.rightBarButtonItems = [barButtonright2,barButtonright]
    }
    
    func renderplayer()
    {
        let button = UIButton.init(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "left_icon"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        button.imageView?.contentMode = UIViewContentMode.scaleToFill
        button.contentEdgeInsets = UIEdgeInsetsMake(0, -25, 0, -10)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        // Do any additional setup after loading the view.
        
        let image : UIImage = UIImage(named: "league")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
        
        
        let buttonsearchright = UIButton.init(type: .custom)
        //set image for button
        buttonsearchright.setImage(UIImage(named: "add_icon"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        buttonsearchright.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        buttonsearchright.imageView?.contentMode = UIViewContentMode.scaleToFill
        buttonsearchright.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        let barButtonright = UIBarButtonItem(customView: buttonsearchright)
        
        
        let buttonsearchright2 = UIButton.init(type: .custom)
        //set image for button
        buttonsearchright2.setImage(UIImage(named: "search"), for: UIControlState.normal)
        //add function for button
        
        //set frame
        buttonsearchright2.frame = CGRect(x: 0, y: 0, width: 50, height: 40)
        buttonsearchright2.imageView?.contentMode = UIViewContentMode.scaleToFill
        buttonsearchright2.contentEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0)
        let barButtonright2 = UIBarButtonItem(customView: buttonsearchright2)
        
        self.navigationItem.rightBarButtonItems = [barButtonright2,barButtonright]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
