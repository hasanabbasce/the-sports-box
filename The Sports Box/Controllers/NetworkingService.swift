//
//  Networkservice.swift
//  Sportclub
//
//  Created by Hasan Abbas on 29/12/2017.
//  Copyright © 2017 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct NetworkingService {
    
    
    var databaseRef: DatabaseReference! {
        
        return Database.database().reference()
    }
    
    var storageRef: StorageReference! {
        
        return Storage.storage().reference()
    }
    
    
    func signUpApp(user:Useri,pictureData: Data?,password:String!, completion: @escaping ([String:Any])->())
    {
        self.signUpfirebase(fullname: user.fullname, pictureData: pictureData, email: user.email, password: password) { (response) in
            if(response["Data"] != nil)
            {
                let fireuser = response["Data"] as! User
                var newuser = user
                newuser.uid = fireuser.uid
                if(fireuser.photoURL != nil)
                {
                    newuser.profile_img = fireuser.photoURL!.absoluteString
                }
                self.addUserInfotodb(useri: newuser, completion: { (response2) in
                    if(response["Data"] != nil)
                    {
                        self.signIn(email: newuser.email!, password: password, completion: { (response3) in
                            completion(response3)
                        })
                    }
                    else
                    {
                        completion(response2)
                    }
                })
            }
            else
            {
                completion(response)
            }
        }
    }
    
    func signUpfirebase(fullname:String!,pictureData: Data?, email: String!,password:String!, completion: @escaping ([String:Any])->()){
        
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            if let error = error {
                completion(["Error" : error.localizedDescription ])
            }else {
                let changeRequest = user?.createProfileChangeRequest()
                changeRequest?.displayName = "\(fullname) "
                
                if(pictureData != nil)
                {
                    let profilePicturePath = "profileImage/\(user!.uid)image.jpg"
                    self.setpic(PicturePath: profilePicturePath, pictureData: pictureData!, completion: { (response) in
                        changeRequest!.photoURL = response["Data"] as? URL
                        changeRequest?.commitChanges(completion: { (error) in
                            if error != nil {
                                completion(["Error" : error?.localizedDescription ,"Data" : user! ])
                            }
                            else
                            {
                                completion(["Data" : user! ])
                            }
                        })
                    })
                    
                }
                else
                {
                    changeRequest?.commitChanges(completion: { (error) in
                        if error != nil {
                            completion(["Error" : error?.localizedDescription ,"Data" : user! ])
                        }
                        else
                        {
                            completion(["Data" : user! ])
                        }
                    })
                }
                
                
            }
        })
    }
    
    
    func setpic(PicturePath: String!,pictureData: Data, completion: @escaping ([String:Any])->())
    {
        let PictureRef = storageRef.child(PicturePath)
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"
        PictureRef.putData(pictureData, metadata: metaData) { (newMetadata, error) in
            if let error = error {
                completion(["Error" : error.localizedDescription ])
            }
            else
            {
                if let url = newMetadata?.downloadURL() {
                    completion(["Data" : url as Any ])
                }
            }
        }
    }
    
    
    func addUserInfotodb(useri:Useri! ,completion: @escaping ([String:Any])->())
    {
        if(useri.uid != nil)
        {
            let userRef = self.databaseRef.child("users").child(useri.uid!)
            userRef.setValue(useri.toAnyObject()) { (error, ref) in
                if error != nil {
                    completion(["Error" : error?.localizedDescription])
                }
                else
                {
                    completion(["Data" : useri])
                }
            }
        }
        else
        {
            completion(["Error" : "User Not Found" ])
        }
    }
    
    func updateUserInfotodb(useri:Useri!,completion: @escaping ([String:Any])->())
    {
        if(useri.uid != nil)
        {
            let userRef = self.databaseRef.child("users").child(useri.uid!)
            userRef.updateChildValues(useri.toAnyObject()) { (error, ref) in
                if error != nil {
                    completion(["Error" : error?.localizedDescription])
                }
                else
                {
                    completion(["Data" : useri])
                }
            }
        }
        else
        {
            completion(["Error" : "User Not Found" ])
        }
    }
    
    
    func signIn(email: String, password: String,completion: @escaping ([String:Any])->()){
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if error == nil {
                if let user = user {
                    completion(["Data" : user])
                }
                
            }else {
                completion(["Error" : error!.localizedDescription])
            }
        })
    }
    
    func fetchUserfromdb(uid: String,completion: @escaping ([String:Any])->()){
        
        let UserRef =  databaseRef.child("users").child(uid)
        
        UserRef.observeSingleEvent(of: .value, with: { (user) in
            if(user.exists())
            {
                let  user: Useri = Useri(snapshot: user)!
                completion(["Data" : user])
            }
            else
            {
                completion(["Error" : "User Not exist"])
            }
            
        }) { (error) in
            completion(["Error" : error.localizedDescription])
        }
    }
    
    
    func logOut(completion: ()->()){
        
        
        if Auth.auth().currentUser != nil {
            
            do {
                
                try Auth.auth().signOut()
                completion()
            }
                
            catch let error {
                //  let alertView = SCLAlertView()
                // alertView.showError("OOPS", subTitle: (error.localizedDescription))
                print("Failed to log out user: \(error.localizedDescription)")
            }
        }
        
    }
    
    func addTeamInfotodb(team:Team! ,completion: @escaping ([String:Any])->())
    {
        if(team.tid != nil)
        {
            let teamRef = self.databaseRef.child("teams").child(team.tid!)
            teamRef.setValue(team.toAnyObject()) { (error, ref) in
                if error != nil {
                    completion(["Error" : error?.localizedDescription])
                }
                else
                {
                    completion(["Data" : team])
                }
            }
        }
        else
        {
            completion(["Error" : "Team Not Found" ])
        }
    }
    
    func updateTeamInfotodb(team:Team!,completion: @escaping ([String:Any])->())
    {
        if(team.tid != nil)
        {
            let teamRef = self.databaseRef.child("teams").child(team.tid!)
            teamRef.updateChildValues(team.toAnyObject()) { (error, ref) in
                if error != nil {
                    completion(["Error" : error?.localizedDescription])
                }
                else
                {
                    completion(["Data" : team])
                }
            }
        }
        else
        {
            completion(["Error" : "Team Not Found" ])
        }
    }
    
    func fetchTeamfromdb(tid: String,completion: @escaping ([String:Any])->()){
        
        let TeamRef =  databaseRef.child("teams").child(tid)
        
        TeamRef.observeSingleEvent(of: .value, with: { (team) in
            if(team.exists())
            {
                let  team: Team = Team(snapshot: team)!
                completion(["Data" : team])
            }
            else
            {
                completion(["Error" : "Team Not exist"])
            }
            
        }) { (error) in
            completion(["Error" : error.localizedDescription])
        }
    }
    
    func userfetchCreatedTeamsfromdb(uid: String,completion: @escaping ([String:Any])->()){
        
        databaseRef.child("teams").queryOrdered(byChild:  "uid").queryEqual(toValue: uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            var resultArray = [Team]()
            // let dgroup: DispatchGroup = DispatchGroup()
            for teamsnap in snapshot.children.allObjects as! [DataSnapshot] {
                //  dgroup.enter()
                let team = Team(snapshot: teamsnap)
                resultArray.append(team!)
                
            }
            //            dgroup.notify(queue: DispatchQueue.main) {
            //                completion(resultArray)
            //            }
            completion(["Data" : resultArray])
            
        }) { (error) in
            completion(["Error" : error.localizedDescription])
        }
        
    }
    
    func addLeagueInfotodb(league:League! ,completion: @escaping ([String:Any])->())
    {
        if(league.lid != nil)
        {
            let LeagueRef = self.databaseRef.child("leagues").child(league.lid!)
            LeagueRef.setValue(league.toAnyObject()) { (error, ref) in
                if error != nil {
                    completion(["Error" : error?.localizedDescription])
                }
                else
                {
                    completion(["Data" : league])
                }
            }
        }
        else
        {
            completion(["Error" : "League Not Found" ])
        }
    }
    
    func updateLeagueInfotodb(league:League!,completion: @escaping ([String:Any])->())
    {
        if(league.lid != nil)
        {
            let LeagueRef = self.databaseRef.child("leagues").child(league.lid!)
            LeagueRef.updateChildValues(league.toAnyObject()) { (error, ref) in
                if error != nil {
                    completion(["Error" : error?.localizedDescription])
                }
                else
                {
                    completion(["Data" : league])
                }
            }
        }
        else
        {
            completion(["Error" : "League Not Found" ])
        }
    }
    
    func fetchLeaguefromdb(lid: String,completion: @escaping ([String:Any])->()){
        
        let LeagueRef =  databaseRef.child("leagues").child(lid)
        
        LeagueRef.observeSingleEvent(of: .value, with: { (league) in
            if(league.exists())
            {
                let  league: League = League(snapshot: league)!
                completion(["Data" : league])
            }
            else
            {
                completion(["Error" : "League Not exist"])
            }
            
        }) { (error) in
            completion(["Error" : error.localizedDescription])
        }
    }
    
    
    func userfetchCreatedLeaguesfromdb(uid: String,completion: @escaping ([String:Any])->()){
        
        databaseRef.child("leagues").queryOrdered(byChild:  "uid").queryEqual(toValue: uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            var resultArray = [League]()
            // let dgroup: DispatchGroup = DispatchGroup()
            for leaguesnap in snapshot.children.allObjects as! [DataSnapshot] {
                //  dgroup.enter()
                let league = League(snapshot: leaguesnap)
                resultArray.append(league!)
                
            }
            //            dgroup.notify(queue: DispatchQueue.main) {
            //                completion(resultArray)
            //            }
            completion(["Data" : resultArray])
            
        }) { (error) in
            completion(["Error" : error.localizedDescription])
        }
        
    }
    
   
    
    func joinleaguedb(joinleague:Leagues_join! ,completion: @escaping ([String:Any])->())
    {
        if(joinleague.jlid != nil)
        {
            let joinleagueRef = self.databaseRef.child("leagues_join").child(joinleague.jlid!)
            joinleagueRef.setValue(joinleague.toAnyObject()) { (error, ref) in
                if error != nil {
                    completion(["Error" : error?.localizedDescription])
                }
                else
                {
                    completion(["Data" : joinleague])
                }
            }
        }
        else
        {
            completion(["Error" : "Join League Not Found" ])
        }
    }
    
    func jointeamdb(jointeam:Teams_join! ,completion: @escaping ([String:Any])->())
    {
        if(jointeam.jtid != nil)
        {
            let jointeamRef = self.databaseRef.child("teams_join").child(jointeam.jtid!)
            jointeamRef.setValue(jointeam.toAnyObject()) { (error, ref) in
                if error != nil {
                    completion(["Error" : error?.localizedDescription])
                }
                else
                {
                    completion(["Data" : jointeam])
                }
            }
        }
        else
        {
            completion(["Error" : "Join Team Not Found" ])
        }
    }
    
    func userfetchjointeams(uid: String,completion: @escaping ([String:Any])->()){
        databaseRef.child("teams_join").queryOrdered(byChild:  "uid").queryEqual(toValue: uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            var resultArray = [Team]()
            let dgroup: DispatchGroup = DispatchGroup()
            for jointeamsnap in snapshot.children.allObjects as! [DataSnapshot] {
                dgroup.enter()
                let jointeam = Teams_join(snapshot: jointeamsnap)
                let teamRef = self.databaseRef.child("teams").child((jointeam?.tid)!)
                
                teamRef.observeSingleEvent(of: .value, with: { (team) in
                    
                    let teamm: Team = Team(snapshot: team)!
                    resultArray.append(teamm)
                    dgroup.leave()
                }){ (error) in
                    //completion(["Error" : error.localizedDescription ])
                    dgroup.leave()
                }
                
            }
            
            
            dgroup.notify(queue: DispatchQueue.main) {
                completion(["Data" : resultArray])
            }
            
            
        }) { (error) in
            completion(["Error" : error.localizedDescription])
        }
        
    }
    
    
    func userfetchjoinleagues(uid: String,completion: @escaping ([String:Any])->()){
        databaseRef.child("leagues_join").queryOrdered(byChild:  "uid").queryEqual(toValue: uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            var resultArray = [League]()
            let dgroup: DispatchGroup = DispatchGroup()
            for joinleaguesnap in snapshot.children.allObjects as! [DataSnapshot] {
                dgroup.enter()
                let joinleague = Leagues_join(snapshot: joinleaguesnap)
                let leagueRef = self.databaseRef.child("leagues").child((joinleague?.lid)!)
                
                leagueRef.observeSingleEvent(of: .value, with: { (league) in
                    
                    let leaguee: League = League(snapshot: league)!
                    resultArray.append(leaguee)
                    dgroup.leave()
                }){ (error) in
                    //completion(["Error" : error.localizedDescription ])
                    dgroup.leave()
                }
                
            }
            
            
            dgroup.notify(queue: DispatchQueue.main) {
                completion(["Data" : resultArray])
            }
            
            
        }) { (error) in
            completion(["Error" : error.localizedDescription])
        }
        
    }
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

