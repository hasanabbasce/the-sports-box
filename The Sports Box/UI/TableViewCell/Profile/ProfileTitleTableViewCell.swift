//
//  ProfileTitleTableViewCell.swift
//  The Sports Box
//
//  Created by Admin on 24/05/18.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ProfileTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var message: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
