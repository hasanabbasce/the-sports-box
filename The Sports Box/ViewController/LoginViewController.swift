//
//  LoginViewController.swift
//  The Sports Box
//
//  Created by Hasan Abbas on 24/04/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoginViewController: UIViewController,NVActivityIndicatorViewable {
    
  
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    var networkingService = NetworkingService()
    
    let activityData = ActivityData()
    
    @IBAction func login(_ sender: Any) {
        
        if(validatefields())
        {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

            networkingService.signIn(email: email.text!, password: password.text!) { (response) in
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                print(response)
                if(response["Data"] != nil)
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.takeToHome()
                }
                if(response["Error"] != nil)
                {
                    let alert = UIAlertController(title: "Error Occured ", message: response["Error"] as! String  , preferredStyle: UIAlertControllerStyle.alert)
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            print("dissmis alert")
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                        }}))
                }
                
            }
            
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func validatefields() ->Bool
    {
        let emailval = email.text
        let passwordval = password.text
       
        var noerror : Bool = true
        var error_message : String = ""
        if(emailval  == "" && noerror)
        {
            noerror = false
            error_message = "Email required"
            
        }
        else if(!isValidEmail(testStr: emailval!) && noerror)
        {
            noerror = false
            error_message = "Please input correct Email address "
            
        }
        if(passwordval == "" && noerror)
        {
            noerror = false
            error_message = "Password required"
            
        }
        
        if(!noerror)
        {
            let alert = UIAlertController(title: "Error", message: error_message, preferredStyle: UIAlertControllerStyle.alert)
            
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("ok")
                case .cancel:
                    print("cancel")
                case .destructive:
                    print("destructive")
                }}))
            return false
        }
        else
        {
            return true
        }
        //return false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
