//
//  ProfileViewController.swift
//  The Sports Box
//
//  Created by Hasan Abbas on 24/04/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var tableView: UITableView!
    // MARK: UITableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 10;
        }
        else
        {
            return 1;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTitleTableViewCell", for: indexPath)
              //  cell.message.text = "Message by Sohrab"
                
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCoverImageTableViewCell", for: indexPath)
                //set the data here
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDataTableViewCell", for: indexPath)
                //set the data here
                return cell
            }
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDataTableViewCell", for: indexPath)
            //set the data here
            return cell
        }
    }
    
    // MARK: UITableView Delegates
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.imgBannerText.image = UIImage(named: "player_banner_text")
        bannerView.bannerType = BannerType.Profile.rawValue
        bannerView.btnAction.setBackgroundImage(UIImage.init(named: "settings_icon"), for: UIControlState.normal)

        registerTableCells()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func registerTableCells()
    {
        self.tableView.register(UINib(nibName: "ProfileTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTitleTableViewCell")
        self.tableView.register(UINib(nibName: "ProfileCoverImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileCoverImageTableViewCell")
        self.tableView.register(UINib(nibName: "ProfileDataTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileDataTableViewCell")
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension

    }
}
