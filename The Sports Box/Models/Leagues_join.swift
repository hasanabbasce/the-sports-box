//
//  Useri.swift
//  Sportclub
//
//  Created by Hasan Abbas on 05/01/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Firebase

struct Leagues_join {
    
    var jlid: String?
    var lid: String?
    var uid: String?
    var jlstatus: String?
    
    var ref: DatabaseReference?
    var key: String = ""
    
    var post_date: NSNumber?
    
    
    init?(snapshot: DataSnapshot){
        
        
        self.jlid = (snapshot.value as! NSDictionary)["jlid"] as? String ?? ""
        self.lid = (snapshot.value as! NSDictionary)["lid"] as? String ?? ""
        self.uid = (snapshot.value as! NSDictionary)["uid"] as? String ?? ""
        self.jlstatus = (snapshot.value as! NSDictionary)["jlstatus"] as? String ?? ""
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.post_date = (snapshot.value as! NSDictionary)["post_date"] as? NSNumber ?? 0
        
    }
    
    
    init(initdic: [String: Any]){
        
        self.ref = Database.database().reference()
        
        if(initdic["jlid"] != nil)
        {
            self.jlid = initdic["jlid"]! as? String
        }
        if(initdic["lid"] != nil)
        {
            self.lid = initdic["lid"]! as? String
        }
        if(initdic["uid"] != nil)
        {
            self.uid = initdic["uid"]! as? String
        }
        if(initdic["jlstatus"] != nil)
        {
            self.jlstatus = initdic["jlstatus"]! as? String
        }
        
        if(initdic["post_date"] != nil)
        {
            self.post_date = initdic["post_date"]! as? NSNumber
        }
        
        
    }
    
    
    func toAnyObject() -> [String: Any]{
        var initdic = [String: Any]()
        
        if(self.jlid != nil)
        {
            initdic["jlid"] = self.jlid!
        }
        if(self.lid != nil)
        {
            initdic["lid"] = self.lid!
        }
        if(self.uid != nil)
        {
            initdic["uid"] = self.uid!
        }
        if(self.jlstatus != nil)
        {
            initdic["jlstatus"] = self.jlstatus!
        }
        if(self.post_date != nil)
        {
            initdic["postDate"] = self.post_date!
        }
        return initdic
    }
}





