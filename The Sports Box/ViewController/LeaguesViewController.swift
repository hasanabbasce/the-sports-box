//
//  LeaguesViewController.swift
//  The Sports Box
//
//  Created by Hasan Abbas on 24/04/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import Firebase

class LeaguesViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var bannerView: BannerView!
    
    var networkingService = NetworkingService()
    
    var JoinedLeagues: [League] = []
    var MyLeagues: [League] = []
    
    
    func renderleagues()
    {
        networkingService.userfetchjoinleagues(uid: (Auth.auth().currentUser?.uid)!) { (response) in
            print(response)
            print("here")
        }
        
        networkingService.userfetchCreatedLeaguesfromdb(uid: (Auth.auth().currentUser?.uid)!) { (response) in
            print(response)
            print("here")
            
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.imgBannerText.image = UIImage(named: "leagues_banner_text")
        bannerView.bannerType = BannerType.Leagues.rawValue
        bannerView.btnAction.setBackgroundImage(UIImage.init(named: "add_icon"), for: UIControlState.normal)

        tableview.register(UINib(nibName: "LeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "LeagueTableViewCell")
        tableview.dataSource = self
        tableview.delegate = self
        renderleagues()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 400
    }
    
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell", for: indexPath) as! LeagueTableViewCell
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        myLabel.center = CGPoint(x: self.view.frame.width/2, y: 20)
        myLabel.textAlignment = .center
        myLabel.text = "sadsadsd"
        myLabel.textColor = UIColor.white
        vw.addSubview(myLabel)
        vw.backgroundColor = hexStringToUIColor(hex: "#D6B05E")
        // myLabel.center = CGPoint(x: vw., y: 0)
        return vw
    }
    
    

}
